import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { User } from '../database/entities/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { CreateUserDTO } from './models/user-create.dto';
import * as bcrypt from 'bcrypt';

describe('UsersService', () => {
  let service: UsersService;
  const usersRepo: Partial<Repository<User>> = {
    create: jest.fn(),

    find: jest.fn(),

    save: jest.fn(),

    findOne: jest.fn(),

    count: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepo,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('getUser() should ', () => {
    it('call findUserById() once with correct input data', async () => {
      // Arrange
      const mockUserId = '1';
      const mockUser = new User();
      mockUser.id = 1;
      const mockServiceMethod = jest
        .spyOn(service, 'findUserById')
        .mockResolvedValue(mockUser);
      // Act
      await service.findUserById(+mockUserId);
      // Assert
      expect(mockServiceMethod).toHaveBeenCalledTimes(1);
      expect(mockServiceMethod).toHaveBeenCalledWith(+mockUserId);
    });

    it('return result of findUserById() execution', async () => {
      // Arrange
      const mockUserId = '1';
      const mockUser = new User();
      mockUser.id = 1;
      jest.spyOn(service, 'findUserById').mockResolvedValue(mockUser);
      // Act
      const result = await service.findUserById(+mockUserId);
      // Assert
      expect(result).toEqual(mockUser);
    });
  });
  //End of unit tests for this method
  describe('findUserById() should ', () => {
    it('call usersRepository.findOne() once with correct input data', async () => {
      // Arrange
      const mockUserId = '1';
      const mockUser = new User();
      const mockFindOptions = {
        id: +mockUserId,
        dateDeleted: null,
      };
      const mockRepoMethod = jest
        .spyOn(usersRepo, 'findOne')
        .mockResolvedValue(mockUser);
      // Act
      await service.findUserById(+mockUserId);
      // Assert
      expect(mockRepoMethod).toHaveBeenCalledTimes(1);
      expect(mockRepoMethod).toHaveBeenCalledWith(mockFindOptions);
    });

    it(`throw error if findOne() returns a falsy value (i.e if user with the passed id is not found`, async () => {
      // Arrange
      const mockUserId = '1';
      jest.spyOn(usersRepo, 'findOne').mockResolvedValue(undefined);
      // Act & Assert
      expect(service.findUserById(+mockUserId)).rejects.toThrow(
        NotFoundException,
      );
    });

    it('return result of usersRepository.findOne() execution', async () => {
      // Arrange
      const mockUser = new User();
      const mockUserId = '1';
      jest.spyOn(usersRepo, 'findOne').mockResolvedValue(mockUser);
      // Act
      const result = await service.findUserById(+mockUserId);
      // Assert
      expect(result).toEqual(mockUser);
    });
  });
  //End of unit tests for this method

  describe('createUser() should ', () => {
    it('call usersRepository.findOne() once with correct input data', async () => {
      // Arrange
      const mockNewUser = new CreateUserDTO();
      const mockUser = new User();
      const mockFindOptions = {
        email: mockNewUser.email,
        dateDeleted: null,
      };
      const mockRepoMethod = jest
        .spyOn(usersRepo, 'findOne')
        .mockResolvedValue(undefined);
      jest.spyOn(usersRepo, 'create').mockReturnValue(mockUser);
      jest.spyOn(bcrypt, 'hash').mockReturnValue(undefined);

      // Act
      await service.createUser(mockNewUser);
      // Assert
      expect(mockRepoMethod).toHaveBeenCalledTimes(1);
      expect(mockRepoMethod).toHaveBeenCalledWith(mockFindOptions);
    });

    it(`throw new BadRequestException if user with the same email is found in the database`, async () => {
      // Arrange
      const mockNewUser = new CreateUserDTO();
      const mockUser = new User();
      jest.spyOn(usersRepo, 'findOne').mockResolvedValue(mockUser);
      // Act & Assert
      expect(service.createUser(mockNewUser)).rejects.toThrow(
        BadRequestException,
      );
    });

    it('call usersRepository.create() once with correct input data', async () => {
      // Arrange
      const mockNewUser = new CreateUserDTO();
      const mockUser = new User();
      const mockRepoMethod = jest
        .spyOn(usersRepo, 'create')
        .mockReturnValue(mockUser);
      jest.spyOn(usersRepo, 'findOne').mockResolvedValue(undefined);
      // Act
      await service.createUser(mockNewUser);
      // Assert
      expect(mockRepoMethod).toHaveBeenCalledTimes(1);
      expect(mockRepoMethod).toHaveBeenCalledWith(mockNewUser);
    });

    it('call usersRepository.save() once with correct input data', async () => {
      // Arrange
      const mockNewUser = new CreateUserDTO();
      const mockUser = new User();
      mockUser.visited = Promise.resolve([]);
      mockUser.reviews = Promise.resolve([]);
      jest.spyOn(usersRepo, 'findOne').mockResolvedValue(undefined);
      jest.spyOn(bcrypt, 'hash').mockReturnValue(undefined);
      const mockRepoMethod = jest.spyOn(usersRepo, 'save');

      // Act
      await service.createUser(mockNewUser);

      // Assert
      expect(mockRepoMethod).toHaveBeenCalledTimes(1);
      expect(mockRepoMethod).toHaveBeenCalledWith(mockUser);
    });

    it('return result of usersRepository.create() execution', async () => {
      // Arrange
      // Act
      // Assert
    });
  });
  //End of unit tests for this method

  describe(' () should ', () => {
    it('call () once with correct input data', async () => {
      // Arrange
      // Act
      // Assert
    });

    it(`throw error under some condition`, async () => {
      // Arrange
      // Act & Assert
    });

    it('return result of () execution', async () => {
      // Arrange
      // Act
      // Assert
    });
  });
  //End of unit tests for this method
});
