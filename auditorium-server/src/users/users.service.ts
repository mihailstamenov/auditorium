import {
  Injectable,
  BadRequestException,
  NotFoundException,
  HttpStatus,
  HttpException,
} from '@nestjs/common';
import { User } from '../database/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { CreateUserDTO } from './models/user-create.dto';
import { Repository } from 'typeorm';
import { UserDTO } from './models/user.dto';
import * as bcrypt from 'bcrypt';
import { UserLoginDTO } from './models/user-login.dto';
import { JwtPayload } from 'src/auth/models/jwt-payload';

@Injectable()
export class UsersService {
  public constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  public async getUser(userId: string): Promise<UserDTO> {
    const findUser: User = await this.findUserById(+userId);

    return plainToClass(UserDTO, findUser, { excludeExtraneousValues: true });
  }

  public async createUser(newUser: CreateUserDTO): Promise<UserDTO> {
    const checkUserEmail = await this.usersRepository.findOne({
      email: newUser.email,
      dateDeleted: null,
    });

    if (checkUserEmail) {
      throw new BadRequestException(
        `User with ${checkUserEmail.email} already exists!`,
      );
    }
    const salt = await bcrypt.genSalt();

    const createUser: User = this.usersRepository.create(newUser);

    createUser.password = await bcrypt.hash(newUser.password, salt);
    createUser.visited = Promise.resolve([]);
    createUser.reviews = Promise.resolve([]);

    await this.usersRepository.save(createUser);

    return plainToClass(UserDTO, createUser, {
      excludeExtraneousValues: true,
    });
  }

  public async findUserById(userId: number): Promise<User> {
    const findUser: User = await this.usersRepository.findOne({
      id: userId,
      dateDeleted: null,
    });
    if (!findUser) {
      throw new NotFoundException(`User with id ${userId} does not exist!`);
    }
    return findUser;
  }

  public async findByLoginCredentials(
    credentials: UserLoginDTO,
  ): Promise<UserDTO> {
    const user: User = await this.usersRepository.findOne({
      where: { username: credentials.username },
    });

    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    const areEqual = await bcrypt.compare(credentials.password, user.password);

    if (!areEqual) {
      throw new HttpException('Invalid password', HttpStatus.UNAUTHORIZED);
    }
    this.usersRepository.save(user);
    return plainToClass(UserDTO, user, {
      excludeExtraneousValues: true,
    });
  }

  public async findByPayload(payload: JwtPayload): Promise<UserDTO> {
    const user: User = await this.usersRepository.findOne({
      where: { id: payload.id },
    });
    return plainToClass(UserDTO, user, {
      excludeExtraneousValues: true,
    });
  }
}
