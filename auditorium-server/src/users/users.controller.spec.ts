import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserDTO } from './models/user.dto';
import { CreateUserDTO } from './models/user-create.dto';

describe('Users Controller', () => {
  let controller: UsersController;
  const usersService: Partial<UsersService> = {
    getUser: jest.fn(),
    createUser: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: usersService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('createUser() should ', () => {
    it('call usersService.createUser() once with correct input data', async () => {
      // Arrange
      const mockServiceMethod = jest.spyOn(usersService, 'createUser');
      const mockNewUser = new CreateUserDTO();
      // Act
      await controller.createUser(mockNewUser);
      // Assert
      expect(mockServiceMethod).toHaveBeenCalledTimes(1);
      expect(mockServiceMethod).toHaveBeenCalledWith(mockNewUser);
    });

    it('return result of usersService.createUser() execution', async () => {
      // Arrange
      const mockNewUser = new CreateUserDTO();
      const mockUser = new UserDTO();
      jest.spyOn(usersService, 'createUser').mockResolvedValue(mockUser);
      // Act
      const result = await controller.createUser(mockNewUser);
      // Assert
      expect(result).toEqual(mockUser);
    });
  });
  // End of unit tests for this method

  describe('getUserById() should ', () => {
    it('call usersService.getUser() once with correct input data', async () => {
      // Arrange
      const mockServiceMethod = jest.spyOn(usersService, 'getUser');
      const mockId = '1';
      // Act
      await controller.getUserById(mockId);
      // Assert
      expect(mockServiceMethod).toHaveBeenCalledTimes(1);
      expect(mockServiceMethod).toHaveBeenCalledWith(mockId);
    });

    it('return result of usersService.getUser() execution', async () => {
      // Arrange
      const mockId = '1';
      const mockUser = new UserDTO();
      mockUser.id = 1;
      jest.spyOn(usersService, 'getUser').mockResolvedValue(mockUser);
      // Act
      const result = await controller.getUserById(mockId);
      // Assert
      expect(result).toEqual(mockUser);
    });
  });
  // End of unit tests for this method
});
