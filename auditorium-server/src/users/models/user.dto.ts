import { Expose } from 'class-transformer';

export class UserDTO {
  @Expose()
  public id: string;
  @Expose()  
  public photo: string;

  @Expose()
  public username: string;

  @Expose()
  public email: string;

  @Expose()
  public dateCreated: Date;
}
