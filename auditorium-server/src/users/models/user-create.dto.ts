import {
  IsString,
  IsNotEmpty,
  IsEmail,
  MaxLength,
  IsUrl,
  IsOptional,
} from 'class-validator';
export class CreateUserDTO {
  @IsUrl()
  @IsOptional()
  public photo: string;

  @IsNotEmpty({
    message: 'Username is a required field.',
  })
  @IsString()
  @MaxLength(20)
  public username: string;
  @IsNotEmpty()
  @IsEmail()
  public email: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(20)
  public password: string;
}
