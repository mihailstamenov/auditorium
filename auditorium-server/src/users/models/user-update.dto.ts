import { Length, IsEmail, IsOptional, IsString, IsUrl } from 'class-validator';

export class UpdateUserDTO {
  @IsOptional()
  @IsUrl()
  public photo?: string;
  @IsOptional()
  @IsEmail()
  public email?: string;

  @IsOptional()
  @IsString()
  public username?: string;

  @IsOptional()
  @Length(4, 20)
  public password?: string;
}
