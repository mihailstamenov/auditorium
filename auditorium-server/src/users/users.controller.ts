import {
  Controller,
  HttpCode,
  Post,
  Body,
  HttpStatus,
  Get,
  Param,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDTO } from './models/user-create.dto';
import { UserDTO } from './models/user.dto';

@Controller('users')
export class UsersController {
  public constructor(private readonly usersService: UsersService) {}

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  public async createUser(@Body() newUser: CreateUserDTO): Promise<UserDTO> {
    return await this.usersService.createUser(newUser);
  }

  @Get(':userId')
  @HttpCode(HttpStatus.FOUND)
  public async getUserById(@Param('userId') userId: string): Promise<UserDTO> {
    return this.usersService.getUser(userId);
  }
}
