import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import { Review } from './review.entity';
import { User } from './user.entity';

@Entity('theaters')
export class Theater {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    type: 'nvarchar',
    nullable: false,
    charset: 'utf8mb4',
    unique: true,
  })
  public name: string;

  @Column({ type: 'nvarchar' })
  public logo: string;

  @Column({ type: 'nvarchar', nullable: false, charset: 'utf8mb4' })
  public city: string;

  @Column({ type: 'nvarchar', nullable: false, charset: 'utf8mb4' })
  public streetAddress: string;

  @Column({ type: 'nvarchar', unique: true })
  public url: string;

  @Column({ type: 'int', default: 0 })
  public starRating: number;

  @Column({ type: 'nvarchar', charset: 'utf8mb4' })
  public about: string;

  @OneToMany(
    type => Review,
    review => review.theater,
    { lazy: true },
  )
  public reviews: Promise<Review[]>;

  @ManyToMany(
    type => User,
    user => user.visited,
    {
      cascade: true,
    },
  )
  public visitors: Promise<User[]>;

  @DeleteDateColumn()
  public dateDeleted: Date;
}
