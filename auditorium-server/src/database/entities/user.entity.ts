import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  CreateDateColumn,
  ManyToMany,
  JoinTable,
  DeleteDateColumn,
} from 'typeorm';
import { Theater } from './theater.entity';
import { Review } from './review.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({
    type: 'nvarchar',
    nullable: false,
    unique: true,
    charset: 'utf8mb4',
  })
  public username: string;

  @Column({ type: 'nvarchar', nullable: false, unique: true })
  public email: string;

  @Column({ type: 'nvarchar', nullable: false })
  public password: string;

  @Column({ type: 'nvarchar' })
  public photo: string;

  @ManyToMany(
    type => Theater,
    theater => theater.visitors,
  )
  @JoinTable()
  public visited: Promise<Theater[]>;

  @OneToMany(
    type => Review,
    review => review.user,
    { lazy: true },
  )
  public reviews: Promise<Review[]>;

  @CreateDateColumn()
  public dateCreated: Date;

  @DeleteDateColumn()
  public dateDeleted: Date;
}
