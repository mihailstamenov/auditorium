import { Theater } from './theater.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('reviews')
export class Review {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: 'nvarchar', nullable: false, charset: 'utf8mb4' })
  public comment: string;

  @Column({ type: 'int', default: 0 })
  public starRating: number;

  @Column({ default: false })
  public recommended: boolean;

  @CreateDateColumn()
  public datePublished: Date;

  @UpdateDateColumn()
  public dateModified: Date;

  @DeleteDateColumn()
  public dateDeleted: Date;

  @ManyToOne(
    type => User,
    user => user.reviews,
    { lazy: true },
  )
  public user: Promise<User>;

  @ManyToOne(
    type => Theater,
    theater => theater.reviews,
    { lazy: true },
  )
  public theater: Promise<Theater>;
}
