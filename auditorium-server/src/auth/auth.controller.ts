import { AuthService } from './auth.service';
import {
  Controller,
  Post,
  Body,
  Delete,
  // HttpException,
  // HttpStatus,
} from '@nestjs/common';
import { UserLoginDTO } from '../users/models/user-login.dto';
// import { RegistrationStatus } from './models/registration-status';
// import { CreateUserDTO } from '../users/models/user-create.dto';
import { LoginStatus } from './models/login-status';
import { Token } from 'src/common/decorators/token.decorator';
@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  // @Post('register')
  // public async register(
  //   @Body() createUser: CreateUserDTO,
  // ): Promise<RegistrationStatus> {
  //   const result: RegistrationStatus = await this.authService.register(
  //     createUser,
  //   );
  //   if (!result.success) {
  //     throw new HttpException(result.message, HttpStatus.BAD_REQUEST);
  //   }
  //   return result;
  // }

  @Post('login')
  async login(
    @Body()
    userLoginCredentials: UserLoginDTO,
  ): Promise<LoginStatus> {
    return await this.authService.login(userLoginCredentials);
  }

  @Delete('logout')
  public async logout(@Token() token: string): Promise<any> {
    return this.authService.logout(token);
  }
}
