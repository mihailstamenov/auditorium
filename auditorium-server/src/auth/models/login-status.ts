export class LoginStatus {
  public message: string;
  public accessToken: string;
}
