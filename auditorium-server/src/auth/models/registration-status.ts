export class RegistrationStatus {
  public success: boolean;
  public message: string;
}
