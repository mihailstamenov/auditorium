import { UserLoginDTO } from '../users/models/user-login.dto';
import { UsersService } from '../users/users.service';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './models/jwt-payload';
// import { CreateUserDTO } from '../users/models/user-create.dto';
// import { RegistrationStatus } from './models/registration-status';
import { LoginStatus } from './models/login-status';
import { LogoutStatus } from './models/logout-status';
import { UserDTO } from 'src/users/models/user.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) {}

  private readonly _blacklist: string[] = [];

  // async register(
  //   userRegCredentials: CreateUserDTO,
  // ): Promise<RegistrationStatus> {
  //   let status: RegistrationStatus = {
  //     success: true,
  //     message: 'user registered',
  //   };
  //   try {
  //     await this.usersService.createUser(userRegCredentials);
  //   } catch (err) {
  //     status = {
  //       success: false,
  //       message: err,
  //     };
  //   }
  //   return status;
  // }

  async login(userLoginCredentials: UserLoginDTO): Promise<LoginStatus> {
    const user: UserDTO = await this.usersService.findByLoginCredentials(
      userLoginCredentials,
    );
    const token = await this._createToken(user);
    return {
      message: `Welcome, ${user.username}!`,
      ...token,
    };
  }

  async logout(token: string): Promise<LogoutStatus> {
    this._blacklist.push(token);

    return {
      message: `Logout successful`,
    };
  }

  async validateUser(payload: JwtPayload): Promise<UserDTO> {
    const user: UserDTO = await this.usersService.findByPayload(payload);
    if (!user) {
      throw new HttpException(
        `Invalid or expired token`,
        HttpStatus.UNAUTHORIZED,
      );
    }
    return user;
  }

  private async _createToken(user: UserDTO): Promise<{ accessToken: string }> {
    const userCredentials: JwtPayload = { ...user };
    const accessToken = await this.jwtService.signAsync(userCredentials);
    return {
      accessToken,
    };
  }
}
