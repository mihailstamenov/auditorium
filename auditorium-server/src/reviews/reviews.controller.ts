import {
  Controller,
  HttpCode,
  Get,
  HttpStatus,
  Put,
  Post,
  Delete,
  Body,
  Param,
  UseGuards,
} from '@nestjs/common';
import { ReviewsService } from './reviews.service';
import { ReviewDTO } from './models/review.dto';
import { CreateReviewDTO } from './models/review-create.dto';
import { AuthGuard } from '@nestjs/passport';
import { UserDTO } from '../users/models/user.dto';
import { User } from '../common/decorators/user.decorator';
import { ReviewActionStatus } from './models/review-action-status';
import { UpdateReviewDTO } from './models/review-update.dto';

@Controller()
export class ReviewsController {
  public constructor(private readonly reviewsService: ReviewsService) {}

  @Get('/theaters/:theaterId/reviews')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getTheaterReviews(
    @Param('theaterId') theaterId: string,
  ): Promise<ReviewDTO[]> {
    return await this.reviewsService.getTheaterReviews(theaterId);
  }

  @Get('/users/:userId/reviews')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getUserReviews(
    @Param('userId') userId: string,
  ): Promise<ReviewDTO[]> {
    return await this.reviewsService.getUserReviews(userId);
  }

  @Post('/theaters/:theaterId/reviews/:userId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  public async addReview(
    @Body() newReview: CreateReviewDTO,
    @Param('theaterId') theaterId: string,
    @User() user: UserDTO,
  ): Promise<ReviewDTO> {
    return await this.reviewsService.addReview(newReview, theaterId, user);
  }

  @Post('/theaters/:theaterId/recommend/:userId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async recommendTheater(
    @Param('theaterId') theaterId: string,
    @Body()
    recommendReview: UpdateReviewDTO,
    @User() user: UserDTO,
  ): Promise<ReviewDTO> {
    return await this.reviewsService.recommendTheater(
      theaterId,
      recommendReview,
      user,
    );
  }

  @Put('users/:userId/reviews/:reviewId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async updateReview(
    @Param('reviewId') reviewId: string,
    @Body() reviewUpdate: UpdateReviewDTO,
    @User() user: UserDTO,
  ): Promise<ReviewDTO> {
    return await this.reviewsService.updateReview(reviewId, reviewUpdate, user);
  }

  @Delete('users/:userId/reviews/:reviewId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async deleteReview(
    @Param('reviewId') reviewId: string,
    @User() user: UserDTO,
  ): Promise<ReviewActionStatus> {
    return await this.reviewsService.deleteReview(reviewId, user);
  }
}
