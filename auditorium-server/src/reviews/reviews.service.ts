import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { Review } from '../database/entities/review.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ReviewDTO } from './models/review.dto';
import { CreateReviewDTO } from './models/review-create.dto';
import { User } from '../database/entities/user.entity';
import { Theater } from '../database/entities/theater.entity';
import { plainToClass } from 'class-transformer';
import { UserDTO } from 'src/users/models/user.dto';
import { ReviewActionStatus } from './models/review-action-status';
import { UpdateReviewDTO } from './models/review-update.dto';

@Injectable()
export class ReviewsService {
  public constructor(
    @InjectRepository(Review)
    private readonly reviewsRepository: Repository<Review>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,

    @InjectRepository(Theater)
    private readonly theatersRepository: Repository<Theater>,
  ) {}

  public async getTheaterReviews(theaterId: string): Promise<ReviewDTO[]> {
    const loadReviews: Review[] = await this.reviewsRepository.find({
      where: {
        theater: +theaterId,
        dateDeleted: null,
      },
    });

    if (!loadReviews) {
      throw new NotFoundException('No reviews for this theater yet');
    }

    return plainToClass(ReviewDTO, loadReviews, {
      excludeExtraneousValues: true,
    });
  }

  public async getUserReviews(userId: string): Promise<ReviewDTO[]> {
    const fetchReviews: Review[] = await this.reviewsRepository.find({
      relations: ['users'],
      where: { user: +userId, dateDeleted: null },
    });

    return plainToClass(ReviewDTO, fetchReviews, {
      excludeExtraneousValues: true,
    });
  }

  public async addReview(
    newReview: CreateReviewDTO,
    theaterId: string,
    user: UserDTO,
  ): Promise<ReviewDTO> {
    const findTheater: Theater = await this.theatersRepository.findOne({
      where: {
        id: +theaterId,
        dateDeleted: null,
      },
    });

    const findUser: User = await this.usersRepository.findOne({
      where: {
        id: user.id,
        dateDeleted: null,
      },
    });

    if (
      (await findTheater.visitors).every(visitor => visitor.id !== findUser.id)
    ) {
      throw new UnauthorizedException(
        `You must have visited this theater before you can leave reviews`,
      );
    }

    const theaterReview: Review = await this.reviewsRepository.create({
      ...newReview,
    });

    theaterReview.user = Promise.resolve(findUser);
    theaterReview.theater = Promise.resolve(findTheater);

    const saveReview: Review = await this.reviewsRepository.save(theaterReview);

    return plainToClass(ReviewDTO, saveReview, {
      excludeExtraneousValues: true,
    });
  }

  public async updateReview(
    reviewId: string,
    reviewUpdate: UpdateReviewDTO,
    user: UserDTO,
  ): Promise<ReviewDTO> {
    const findReview: Review = await this.reviewsRepository.findOne({
      where: {
        id: +reviewId,
        dateDeleted: null,
      },
    });

    if (!findReview) {
      throw new NotFoundException(
        `Review with id: ${reviewId} has not been found`,
      );
    }

    if ((await findReview.user).id !== +user.id) {
      throw new UnauthorizedException(
        `User with id: ${user.id} does not have permissions to update review with id: ${reviewId}`,
      );
    }

    const updateReview: Review = { ...findReview, ...reviewUpdate };

    const updatedReview = await this.reviewsRepository.save(updateReview);

    return plainToClass(ReviewDTO, updatedReview, {
      excludeExtraneousValues: true,
    });
  }

  public async recommendTheater(
    theaterId: string,
    recommendReview: UpdateReviewDTO,
    user: UserDTO,
  ): Promise<ReviewDTO> {
    const createRecommendReview: Review = await this.reviewsRepository.create({
      ...recommendReview,
    });

    const findUser: User = await this.usersRepository.findOne({
      where: {
        id: user.id,
        dateDeleted: null,
      },
    });

    if (!findUser) {
      throw new NotFoundException(`User with id: ${user.id} not found`);
    }

    const findTheater: Theater = await this.theatersRepository.findOne({
      where: {
        id: +theaterId,
        dateDeleted: null,
      },
    });

    if (!findTheater) {
      throw new NotFoundException(`Theater with id: ${theaterId} not found`);
    }

    createRecommendReview.user = Promise.resolve(findUser);
    createRecommendReview.theater = Promise.resolve(findTheater);
    createRecommendReview.recommended = true;
    createRecommendReview.starRating = 5;

    const saveRecommendation = await this.reviewsRepository.save(
      createRecommendReview,
    );

    return plainToClass(ReviewDTO, saveRecommendation, {
      excludeExtraneousValues: true,
    });
  }
  public async deleteReview(
    reviewId: string,
    user: UserDTO,
  ): Promise<ReviewActionStatus> {
    const findReview: Review = await this.reviewsRepository.findOne({
      where: {
        id: +reviewId,
        dateDeleted: null,
      },
    });

    if (!findReview) {
      throw new NotFoundException(`Review with id: ${reviewId} not found`);
    }

    if ((await findReview.user).id !== +user.id) {
      throw new UnauthorizedException(
        `User with id: ${user.id} does not have permissions to update review with id: ${reviewId}`,
      );
    }

    let status: ReviewActionStatus = {
      success: true,
      message: `Review with id: ${reviewId} deleted!`,
    };
    try {
      await this.reviewsRepository.softDelete(findReview.id);
    } catch (err) {
      status = { success: false, message: err };
    }
    return status;
  }
}
