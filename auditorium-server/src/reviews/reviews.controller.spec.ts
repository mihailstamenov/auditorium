import { Test, TestingModule } from '@nestjs/testing';
import { ReviewsController } from './reviews.controller';
import { ReviewsService } from './reviews.service';
import { ReviewDTO } from './models/review.dto';

describe('Reviews Controller', () => {
  let controller: ReviewsController;
  const reviewsService: Partial<ReviewsService> = {
    getTheaterReviews: jest.fn(),
    getUserReviews: jest.fn(),
    updateReview: jest.fn(),
    addReview: jest.fn(),
    deleteReview: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReviewsController],
      providers: [
        {
          provide: ReviewsService,
          useValue: reviewsService,
        },
      ],
    }).compile();

    controller = module.get<ReviewsController>(ReviewsController);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getTheaterReviews() should ', () => {
    it('call theatersService.getTheaterReviews() once with correct input data', async () => {
      const mockTheaterId = '1';
      // Arrange
      const mockServiceMethod = jest.spyOn(reviewsService, 'getTheaterReviews');
      // Act
      await reviewsService.getTheaterReviews(mockTheaterId);
      // Assert
      expect(mockServiceMethod).toHaveBeenCalledTimes(1);
      expect(mockServiceMethod).toHaveBeenCalledWith(mockTheaterId);
    });

    it('return result of theatersService.getTheaterReviews() execution', async () => {
      // Arrange
      const mockReview = new ReviewDTO();
      const mockTheaterId = '1';
      jest
        .spyOn(reviewsService, 'getTheaterReviews')
        .mockResolvedValue([mockReview]);
      // Act
      const result = await controller.getTheaterReviews(mockTheaterId);
      // Assert
      expect(result).toEqual([mockReview]);
    });
  });
  // End of unit tests for method
});
