export class ReviewActionStatus {
  public success: boolean;
  public message: string;
}
