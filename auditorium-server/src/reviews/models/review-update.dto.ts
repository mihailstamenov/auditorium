import {
  IsString,
  IsNumber,
  MaxLength,
  IsOptional,
  Min,
  IsBoolean,
} from 'class-validator';

export class UpdateReviewDTO {
  @IsOptional()
  @IsString()
  @MaxLength(1000)
  public comment?: string;
  @IsOptional()
  @Min(0)
  @IsNumber()
  public starRating?: number;

  @IsBoolean()
  public recommended?: boolean;
}
