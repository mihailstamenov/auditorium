import { Expose } from 'class-transformer';

export class ReviewDTO {
  @Expose()
  public id: number;

  @Expose()
  public comment: string;

  @Expose()
  public starRating: number;

  @Expose()
  public recommended: boolean;

  @Expose()
  public datePublished: Date;
}
