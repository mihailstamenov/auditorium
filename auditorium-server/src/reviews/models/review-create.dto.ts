import { IsString, IsNumber, Min, IsBoolean } from 'class-validator';

export class CreateReviewDTO {
  @IsString()
  public comment?: string;

  @Min(0)
  @IsNumber()
  public starRating: number;

  @IsBoolean()
  public recommended?: boolean;
}
