import { Test, TestingModule } from '@nestjs/testing';
import { ReviewsService } from './reviews.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Review } from '../database/entities/review.entity';
import { Repository } from 'typeorm';

describe('ReviewsService', () => {
  let service: ReviewsService;
  const reviewsRepo: Partial<Repository<Review>> = {
    create: jest.fn(),

    find: jest.fn(),

    save: jest.fn(),

    findOne: jest.fn(),

    count: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReviewsService,
        {
          provide: getRepositoryToken(Review),
          useValue: reviewsRepo,
        },
      ],
    }).compile();

    service = module.get<ReviewsService>(ReviewsService);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('serviceMethod() should ', () => {
    it('call someOtherRepoMethod() once with correct input data', async () => {
      // Arrange
      // Act
      // Assert
    });

    it(`throw error under some condition`, async () => {
      // Arrange
      // Act & Assert
    });

    it('return result of someOtherRepoMethod() execution', async () => {
      // Arrange
      // Act
      // Assert
    });
  });
  // End of unit tests for this method
});
