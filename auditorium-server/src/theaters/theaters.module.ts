import { Module } from '@nestjs/common';
import { TheatersController } from './theaters.controller';
import { TheatersService } from './theaters.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Theater } from '../database/entities/theater.entity';
import { User } from '../database/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Theater, User])],
  controllers: [TheatersController],
  providers: [TheatersService],
})
export class TheatersModule {}
