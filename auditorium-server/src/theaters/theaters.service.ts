import { CreateTheaterDTO } from './models/theater-create.dto';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Theater } from '../database/entities/theater.entity';
import { TheaterDTO } from './models/theater.dto';
import { plainToClass } from 'class-transformer';
import { User } from '../database/entities/user.entity';
import { UserDTO } from 'src/users/models/user.dto';

@Injectable()
export class TheatersService {
  constructor(
    @InjectRepository(Theater)
    private readonly theatersRepository: Repository<Theater>,

    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  public async getAllTheaters(): Promise<TheaterDTO[]> {
    const findTheaters: Theater[] = await this.theatersRepository.find({
      where: {
        dateDeleted: null,
      },
    });

    return plainToClass(TheaterDTO, findTheaters, {
      excludeExtraneousValues: true,
    });
  }

  public async addTheater(newTheater: CreateTheaterDTO): Promise<TheaterDTO> {
    const createTheater: Theater = await this.theatersRepository.create({
      ...newTheater,
    });
    createTheater.reviews = Promise.resolve([]);
    createTheater.visitors = Promise.resolve([]);

    const saveTheater: Theater = await this.theatersRepository.save(
      createTheater,
    );

    return plainToClass(TheaterDTO, saveTheater, {
      excludeExtraneousValues: true,
    });
  }

  public async getTheaterById(theaterId: string): Promise<TheaterDTO> {
    const findTheater: Theater = await this.theatersRepository.findOne({
      id: +theaterId,
      dateDeleted: null,
    });

    if (!findTheater) {
      throw new NotFoundException(
        `Theater with id ${theaterId} has not been found`,
      );
    }

    return plainToClass(TheaterDTO, findTheater, {
      excludeExtraneousValues: true,
    });
  }

  public async visitTheater(
    theaterId: string,
    userId: string,
    user: UserDTO,
  ): Promise<any> {
    const findTheater: Theater = await this.theatersRepository.findOne(
      theaterId,
    );

    if (!findTheater) {
      throw new NotFoundException(
        `Theater with id ${theaterId} has not been found`,
      );
    }

    const findUser: User = await this.usersRepository.findOne(+user.id);

    if (!findUser) {
      throw new NotFoundException(`User with id ${user.id} has not been found`);
    }

    (await findTheater.visitors).push(findUser);

    await this.theatersRepository.save(findTheater);

    return {
      message: `You have marked theater with id: ${theaterId} as visited`,
    };
  }
}
