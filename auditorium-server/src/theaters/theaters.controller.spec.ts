import { Test, TestingModule } from '@nestjs/testing';
import { TheatersController } from './theaters.controller';
import { TheatersService } from './theaters.service';
import { CreateTheaterDTO } from './models/theater-create.dto';
import { TheaterDTO } from './models/theater.dto';

describe('Theaters Controller', () => {
  let controller: TheatersController;
  const theatersService: Partial<TheatersService> = {
    getAllTheaters: jest.fn(),
    getTheaterById: jest.fn(),
    addTheater: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TheatersController],
      providers: [
        {
          provide: TheatersService,
          useValue: theatersService,
        },
      ],
    }).compile();

    controller = module.get<TheatersController>(TheatersController);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  }); // Controller sanity check

  describe('getAllTheaters() should ', () => {
    it('call theatersService.getAllTheaters() once', async () => {
      // Arrange
      const mockServiceMethod = jest.spyOn(theatersService, 'getAllTheaters');
      // Act
      await controller.getAllTheaters();
      // Assert
      expect(mockServiceMethod).toHaveBeenCalledTimes(1);
    });

    it('return result of theatersService.getAllTheaters() execution', async () => {
      // Arrange
      const mockTheater = new TheaterDTO();
      jest
        .spyOn(theatersService, 'getAllTheaters')
        .mockResolvedValue([mockTheater]);
      // Act
      const result = await controller.getAllTheaters();
      // Assert
      expect(result).toEqual([mockTheater]);
    });
  });
  // End of unit tests for getAllTheaters()

  describe('getTheaterById() should ', () => {
    it('call theatersService.getTheaterById() once with correct input id', async () => {
      const mockTheaterId = '1';
      // Arrange
      const mockServiceMethod = jest.spyOn(theatersService, 'getTheaterById');
      // Act
      await controller.getTheaterById(mockTheaterId);
      // Assert
      expect(mockServiceMethod).toHaveBeenCalledTimes(1);
      expect(mockServiceMethod).toHaveBeenCalledWith(mockTheaterId);
    });

    it('return result of theatersService.getTheaterById() execution', async () => {
      // Arrange
      const mockTheater = new TheaterDTO();
      mockTheater.id = 1;
      jest
        .spyOn(theatersService, 'getTheaterById')
        .mockResolvedValue(mockTheater);
      // Act
      const result = await controller.getTheaterById('1');
      // Assert
      expect(result).toEqual(mockTheater);
    });
  });
  // End of unit tests for getTheaterById()

  describe('addTheater() should ', () => {
    it('call theatersService.addTheater() once with correct input data', async () => {
      // Arrange
      const newMockTheater = new CreateTheaterDTO();
      newMockTheater.name = 'test_name';
      newMockTheater.city = 'test_city';
      newMockTheater.streetAddress = 'test_street_address';
      newMockTheater.url = 'http://test-url.org';

      const mockServiceMethod = jest.spyOn(theatersService, 'addTheater');
      // Act
      await controller.addTheater(newMockTheater);
      // Assert
      expect(mockServiceMethod).toHaveBeenCalledTimes(1);
      expect(mockServiceMethod).toHaveBeenCalledWith(newMockTheater);
    });

    it('return result of theatersService.addTheater() execution', async () => {
      // Arrange
      const newMockTheater = new CreateTheaterDTO();
      newMockTheater.name = 'test_name';
      newMockTheater.city = 'test_city';
      newMockTheater.streetAddress = 'test_street_address';
      newMockTheater.url = 'http://test-url.org';
      const mockTheater = new TheaterDTO();

      jest.spyOn(theatersService, 'addTheater').mockResolvedValue(mockTheater);
      // Act
      const result = await controller.addTheater(newMockTheater);
      // Assert
      expect(result).toEqual(mockTheater);
    });
  });
  // End of unit tests for addTheater()

  describe('controllerMethod() should ', () => {
    it('call someOtherMethodFromService() once with correct input data', async () => {
      // Arrange
      // Act
      // Assert
    });

    it('return result of someOtherMethodFromService() execution', async () => {
      // Arrange
      // Act
      // Assert
    });
  });
  // End of unit tests for method
});
