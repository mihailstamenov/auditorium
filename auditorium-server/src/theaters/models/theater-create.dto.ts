import {
  IsString,
  IsUrl,
  IsNotEmpty,
  MaxLength,
  IsOptional,
} from 'class-validator';
export class CreateTheaterDTO {
  @IsUrl()
  public logo: string;

  @IsNotEmpty()
  @IsString()
  public name: string;

  @IsNotEmpty()
  @IsString()
  public city: string;

  @IsNotEmpty()
  @IsString()
  public streetAddress: string;

  @IsOptional()
  @MaxLength(500)
  @IsUrl()
  public url: string;

  @IsString()
  public about: string;
}
