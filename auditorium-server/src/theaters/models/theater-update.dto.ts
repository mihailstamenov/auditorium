import { IsString, IsUrl, MaxLength, IsOptional } from 'class-validator';
export class CreateTheaterDTO {
  @IsOptional()
  @IsUrl()
  public logo?: string;

  @IsOptional()
  @IsString()
  public name?: string;

  @IsOptional()
  @IsString()
  public city?: string;

  @IsOptional()
  @IsString()
  public streetAddress?: string;

  @IsOptional()
  @MaxLength(500)
  @IsUrl()
  public url?: string;

  @IsOptional()
  @IsString()
  public about?: string;
}
