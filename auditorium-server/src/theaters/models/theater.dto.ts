import { Expose } from 'class-transformer';

export class TheaterDTO {
  @Expose()
  public id: number;

  @Expose()
  public logo: string;

  @Expose()
  public name: string;

  @Expose()
  public city: string;

  @Expose()
  public streetAddress: string;

  @Expose()
  public url: string;

  @Expose()
  public starRating: number;

  @Expose()
  public about: string;
}
