import {
  Controller,
  Post,
  HttpCode,
  Body,
  HttpStatus,
  Get,
  Param,
  UseGuards,
  Put,
} from '@nestjs/common';
import { TheatersService } from './theaters.service';
import { TheaterDTO } from './models/theater.dto';
import { CreateTheaterDTO } from './models/theater-create.dto';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../common/decorators/user.decorator';
import { UserDTO } from '../users/models/user.dto';

@Controller('theaters')
export class TheatersController {
  constructor(private readonly theatersService: TheatersService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  public async getAllTheaters(): Promise<TheaterDTO[]> {
    return await this.theatersService.getAllTheaters();
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.CREATED)
  public async addTheater(
    @Body() newTheater: CreateTheaterDTO,
  ): Promise<TheaterDTO> {
    return await this.theatersService.addTheater(newTheater);
  }

  @Get('/:theaterId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.FOUND)
  public async getTheaterById(
    @Param('theaterId') theaterId: string,
  ): Promise<TheaterDTO> {
    return await this.theatersService.getTheaterById(theaterId);
  }

  @Get('/:theaterId/visit/:userId')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.FOUND)
  public async visitTheater(
    @Param('theaterId') theaterId: string,
    @Param('userId') userId: string,
    @User() user: UserDTO,
  ): Promise<any> {
    return await this.theatersService.visitTheater(theaterId, userId, user);
  }
}
