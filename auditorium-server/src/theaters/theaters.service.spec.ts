import { Test, TestingModule } from '@nestjs/testing';
import { TheatersService } from './theaters.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Theater } from '../database/entities/theater.entity';
import { TheaterDTO } from './models/theater.dto';
import { NotFoundException } from '@nestjs/common/exceptions';

describe('TheatersService', () => {
  let service: TheatersService;
  const theatersRepo: Partial<Repository<Theater>> = {
    create: jest.fn(),

    find: jest.fn(),

    save: jest.fn(),

    findOne: jest.fn(),

    count: jest.fn(),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TheatersService,
        {
          provide: getRepositoryToken(Theater),
          useValue: theatersRepo,
        },
      ],
    }).compile();

    service = module.get<TheatersService>(TheatersService);
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  }); // Service sanity check

  describe('getAllTheaters() should', () => {
    it('call theatersRepository.find() once with correct options', async () => {
      // Arrange
      const mockFindOptions = { dateDeleted: null };
      const mockRepoMethod = jest.spyOn(theatersRepo, 'find');

      // Act
      await service.getAllTheaters();

      // Assert
      expect(mockRepoMethod).toHaveBeenCalledTimes(1);
      expect(mockRepoMethod).toHaveBeenCalledWith(mockFindOptions);
    });

    it('return result of theatersRepository.find() execution', async () => {
      // Arrange
      const mockTheater = new Theater();
      const mockTheaterDTO = new TheaterDTO();
      const expected = [mockTheaterDTO];

      jest.spyOn(theatersRepo, 'find').mockResolvedValue([mockTheater]);

      // Act
      const result = await service.getAllTheaters();

      // Assert
      expect(result).toEqual(expect.arrayContaining(expected));
    });
  });
  //End of unit tests for getAllTheaters() method

  describe('getTheaterById() should', () => {
    it('call theatersRepository.findOne() once with correct options', async () => {
      // Arrange
      const mockFindOptions = { id: 1, dateDeleted: null };
      const mockTheater = new Theater();
      mockTheater.id = 1;
      const mockId = '1';
      const mockRepoMethod = jest
        .spyOn(theatersRepo, 'findOne')
        .mockResolvedValue(mockTheater);

      // Act
      await service.getTheaterById(mockId);

      // Assert
      expect(mockRepoMethod).toHaveBeenCalledTimes(1);
      expect(mockRepoMethod).toHaveBeenCalledWith(mockFindOptions);
    });

    it('return result of theatersRepository.findOne() execution', async () => {
      // Arrange
      const mockTheater = new Theater();
      mockTheater.id = 1;
      const mockId = '1';

      jest.spyOn(theatersRepo, 'findOne').mockResolvedValue(mockTheater);

      // Act
      const result = await service.getTheaterById(mockId);

      // Assert
      expect(result).toEqual(mockTheater);
    });

    it('throw if findOne() returns a falsy value (i.e if theater with the passed id is not found)', async () => {
      // Arrange
      const mockId = '1';

      jest.spyOn(theatersRepo, 'findOne').mockResolvedValue(undefined);

      // Act & Assert
      expect(service.getTheaterById(mockId)).rejects.toThrow(NotFoundException);
    });
  });
  //End of unit tests for getTheaterById() method
});
