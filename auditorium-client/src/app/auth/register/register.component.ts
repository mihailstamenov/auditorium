import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  public password: string;
  public userName: string;
  public registerForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly formBuilder: FormBuilder,
    private readonly notificator: NotificatorService,
    private readonly router: Router
  ) {}

  public ngOnInit(): void {
    // this.registerForm = this.formBuilder.group({
    //   username: [
    //     '',
    //     [
    //       Validators.required,
    //       Validators.minLength(2),
    //       Validators.maxLength(12),
    //     ],
    //   ],
    //   password: [
    //     '',
    //     [
    //       Validators.required,
    //       Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/),
    //     ],
    //   ],
    //   email: [
    //     '',
    //     [
    //       Validators.required,
    //       Validators.minLength(2),
    //       Validators.maxLength(24),
    //     ],
    //   ],
    // });
  }
  public register(userName: string, password: string, email: string): void {
    // this.authService.registerUser(userName, password, email).subscribe(
    //   () => {
    //     this.notificator.success('Register successful');
    //     this.router.navigate(['/auth/login']);
    //   },
    //   () => this.notificator.error('This user already exsists')
    // );
  }
}
