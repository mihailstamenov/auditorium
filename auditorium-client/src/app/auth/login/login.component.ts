import { UserLoginDTO } from '../../users/models/user-login.dto';
import { Router } from '@angular/router';
import { NotificatorService } from '../../core/services/notificator.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly formBuilder: FormBuilder,
    private readonly notificator: NotificatorService,
    private readonly router: Router
  ) {}

  public ngOnInit(): void {
    // this.loginForm = this.formBuilder.group({
    //   username: [
    //     '',
    //     [
    //       Validators.required,
    //       Validators.minLength(2),
    //       Validators.maxLength(12),
    //     ],
    //   ],
    //   password: ['', [Validators.required]],
    // });
  }

  public loginUser(user: UserLoginDTO): void {
    // this.authService.login(user).subscribe(
    //   () => {
    //     this.notificator.success('Login successful');
    //     this.router.navigate(['/theaters']);
    //   },
    //   () => this.notificator.error('Invalid username or password')
    // );
  }
}
