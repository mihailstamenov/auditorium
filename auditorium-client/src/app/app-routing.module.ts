import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './common/guards/auth.guard';
import { AllTheatersComponent } from './theaters/all-theaters/all-theaters.component';
import { ViewTheaterComponent } from './theaters/view-theater/view-theater.component';
import { HomeComponent } from './home/home.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';
import { ServerErrorComponent } from './server-error/server-error.component';
import { PageNotFoundComponent } from './not-found/page-not-found.component';
import { UserReviewsComponent } from './users/user-reviews/user-reviews.component.';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },

  {
    path: 'theaters',
    component: AllTheatersComponent,
    // canActivate: [AuthGuard],
  },
  // {
  //   path: 'theaters/:theaterId',
  //   redirectTo: 'theaters/:theaterId/reviews',
  //   pathMatch: 'full',
  // },
  {
    path: 'theaters/:theaterId',
    component: ViewTheaterComponent,
    // canActivate: [AuthGuard],
  },
  // {
  //   path: 'users/:userId',
  //   redirectTo: 'users/:userId/profile',
  //   pathMatch: 'full',
  // },
  {
    path: 'users/:userId/profile',
    component: UserProfileComponent,
    // canActivate: [AuthGuard],
  },

  {
    path: 'users/:userId/reviews',
    component: UserReviewsComponent,
    // canActivate: [AuthGuard],
  },
  {
    path: 'users/:userId/visited',
    component: ViewTheaterComponent,
    // canActivate: [AuthGuard],
  },
  /*  lazy loading

  {
    path: 'theaters',
    loadChildren: () => import('./theaters/theaters.module')
    .then( m => m.TheatersModule),
    canActivate: [AuthGuard],
  },
 {
    path: 'reviews',
    loadChildren: () => import('./reviews/reviews.module')
    .then(m => m.ReviewsModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'users',
    loadChildren: () => import('./skills/skills.module').then(
      m => m.UsersModule
    ), canActivate: [
      AuthGuard,
      RoleGuard
    ],
  }
  */
  { path: 'not-found', component: PageNotFoundComponent },
  { path: 'server-error', component: ServerErrorComponent },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
