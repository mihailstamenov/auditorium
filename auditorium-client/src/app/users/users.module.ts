import { UsersService } from '../core/services/users.service';
import { NgModule } from '@angular/core';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SharedModule } from '../shared/shared.module';

import { UserReviewsComponent } from './user-reviews/user-reviews.component.';
import { CommonModule } from '@angular/common';
import { ReviewsService } from '../core/services/reviews.service';
import { TheatersService } from '../core/services/theaters.service';

@NgModule({
  declarations: [UserProfileComponent, UserReviewsComponent],
  imports: [CommonModule, SharedModule],
  exports: [UserProfileComponent, UserReviewsComponent],
})
export class UsersModule {}
