import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../models/user.dto';
import { UsersService } from '../../core/services/users.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
})
export class UserProfileComponent implements OnInit {
  @Input()
  public user: UserDTO;

  public constructor(
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService,
    private readonly router: Router
  ) {}

  public ngOnInit(): void {
    // this.route.params.subscribe((params) => {
    //   this.usersService.getUser(params.userId).subscribe(
    //     (foundUser) => (this.user = foundUser),
    //     (error) => {
    //       if (error.status === 404) {
    //         this.router.navigate(['/not-found']);
    //       }
    //     }
    //   );
    // });
  }
}
