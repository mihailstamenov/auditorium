export class RegisterUserDTO {
  public username: string;
  public email: string;
  public password: string;
}
