export class UserDTO {
  public id: number;

  public username: string;

  public email: string;

  public dateCreated: Date;
}
