import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../core/services/users.service';
import { ReviewDTO } from '../../theaters/models/review.dto';

@Component({
  selector: 'app-user-reviews',
  templateUrl: './user-reviews.component.html',
  styleUrls: ['./user-reviews.component.css'],
})
export class UserReviewsComponent implements OnInit {
  public userReviews: ReviewDTO[] = [];

  private idOfUser: string;

  public constructor(
    private readonly route: ActivatedRoute,
    private readonly usersService: UsersService
  ) {}

  public ngOnInit(): void {
    // const urlAddres = location.href;
    // const id = urlAddres.split('/').slice(-1).join('');
    // this.idOfUser = id;
    // this.usersService.getAllUserReviews(id).subscribe((foundReviews) => {
    //   this.userReviews = foundReviews;
    // });
  }
  public refresh(reviewId: number): void {
    // this.usersService
    //   .getAllUserReviews(this.idOfUser)
    //   .subscribe((foundReviews) => {
    //     this.userReviews = foundReviews;
    //   });
  }
}
