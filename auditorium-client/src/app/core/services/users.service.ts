import { UserDTO } from './../../users/models/user.dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterUserDTO } from '../../users/models/user-register.dto';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private readonly httpClient: HttpClient) {}

  public registerUser(newUser: RegisterUserDTO): void {}

  public getUser(userId: number): Observable<UserDTO> {
    return this.httpClient.get<UserDTO>(`${CONFIG.DOMAIN_NAME}/theaters`);
  }
}
