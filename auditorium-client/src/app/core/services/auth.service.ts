import { UserDTO } from '../../users/models/user.dto';
import { StorageService } from './storage.service';
import { CONFIG } from '../../config/config';
import { Observable, BehaviorSubject } from 'rxjs';
import { UserLoginDTO } from '../../users/models/user-login.dto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
  //   this.isUserLoggedIn()
  // );
  // private readonly loggedUserSubject$ = new BehaviorSubject<UserDTO>(
  //   this.loggedUser()
  // );

  constructor(
    private readonly httpClient: HttpClient,
    private readonly storageService: StorageService,
    private readonly usersService: UsersService,

    private readonly router: Router
  ) {}

  // public get isLoggedIn$(): Observable<boolean> {
  //   return this.isLoggedInSubject$.asObservable();
  // }

  // public get loggedUser$(): Observable<UserDTO> {
  //   return this.loggedUserSubject$.asObservable();
  // }

  public login(user: UserLoginDTO): void {
    // return this.httpClient
    //   .post<UserLoginDTO>(`${CONFIG.DOMAIN_NAME}/login`, user)
    //   .pipe(
    //     tap(({ accessToken }) => {
    //       const loggedUser = this.jwtService.decodeToken(accessToken);
    //       this.storageService.setItem('token', accessToken);
    //       this.isLoggedInSubject$.next(true);
    //       this.loggedUserSubject$.next(loggedUser);
    //     })
    //   );
  }

  // public getUserDataIfAuthenticated(): UserDTO {
  //   // const token: string = this.storageService.getItem('token');
  //   // if (token && this.jwtService.isTokenExpired(token)) {
  //   //   this.storageService.removeItem('token');
  //   //   return null;
  //   // }
  //   // return token ? this.jwtService.decodeToken(token) : null;
  // }

  // public isUserLoggedIn(): boolean {
  //   return !!this.storageService.getItem('token');
  // }

  // private loggedUser(): UserDTO {
  //   try {
  //     return this.jwtService.decodeToken(this.storageService.getItem('token'));
  //   } catch (error) {
  //     this.isLoggedInSubject$.next(false);

  //     return null;
  //   }
  // }

  // public registerUser(
  //   username: string,
  //   password: string,
  //   email: string
  // ): Observable<any> {
  //   return this.httpClient.post(`${CONFIG.DOMAIN_NAME}/users/register`, {
  //     username,
  //     password,
  //     email,
  //   });
  // }

  // public logOutUser(): void {
  //   this.storageService.removeItem('token');
  //   this.router.navigate(['/home']);
  // }
}
