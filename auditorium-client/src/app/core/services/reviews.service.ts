import { UpdateReviewDTO } from '../../theaters/models/review-update.dto';
import { AddReviewDTO } from '../../theaters/models/review-add.dto';
import { ReviewDTO } from '../../theaters/models/review.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root',
})
export class ReviewsService {
  constructor(private readonly httpClient: HttpClient) {}

  public getTheaterReviews(theaterId: string): Observable<ReviewDTO[]> {
    console.log(theaterId);
    return this.httpClient.get<ReviewDTO[]>(
      `${CONFIG.DOMAIN_NAME}/theaters/${theaterId}/reviews`
    );
  }

  public addReviewToTheater(newReview: AddReviewDTO, theaterId: string): void {}

  public getHighestRatedReviews(): Observable<ReviewDTO[]> {
    return this.httpClient.get<ReviewDTO[]>(
      `${CONFIG.DOMAIN_NAME}/theaters/reviews/highlights`
    );
  }

  public getAllUserReviews(userId: string): Observable<ReviewDTO[]> {
    return this.httpClient.get<ReviewDTO[]>(
      `${CONFIG.DOMAIN_NAME}/users/${userId}/reviews`
    );
  }
  public recommendTheater(
    theaterId: string,
    recommendReview: UpdateReviewDTO
  ): void {}

  public updateReview(reviewId: string, reviewUpdate: UpdateReviewDTO): void {}

  public deleteReview(userId: string, reviewId: string): Observable<any> {
    return this.httpClient.get<ReviewDTO[]>(
      `${CONFIG.DOMAIN_NAME}/users/${userId}/reviews/${reviewId}`
    );
  }
}
