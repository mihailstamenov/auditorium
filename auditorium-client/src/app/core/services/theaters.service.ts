import { TheaterDTO } from '../../theaters/models/theater.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root',
})
export class TheatersService {
  constructor(private readonly httpClient: HttpClient) {}

  public getTheater(theaterId: string): Observable<TheaterDTO> {
    return this.httpClient.get<TheaterDTO>(
      `${CONFIG.DOMAIN_NAME}/theaters/${theaterId}`
    );
  }

  public getRecommendedTheaters(): Observable<TheaterDTO[]> {
    return this.httpClient.get<TheaterDTO[]>(
      `${CONFIG.DOMAIN_NAME}/theaters/recommended`
    );
  }

  public getAllTheaters(): Observable<TheaterDTO[]> {
    return this.httpClient.get<TheaterDTO[]>(`${CONFIG.DOMAIN_NAME}/theaters`);
  }

  public addtheater(theater: TheaterDTO): void {}

  public visitTheater(
    theaterId: string,
    userId: string
  ): Observable<TheaterDTO> {
    return this.httpClient.get<TheaterDTO>(
      `${CONFIG.DOMAIN_NAME}/theaters/${theaterId}/visit/${userId}`
    );
  }
}
