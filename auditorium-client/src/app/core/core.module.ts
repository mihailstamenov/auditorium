import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';
import { NotificatorService } from './services/notificator.service';
import { AuthGuard } from '../common/guards/auth.guard';
import { ToastrModule } from 'ngx-toastr';
import { ReviewsService } from './services/reviews.service';
import { TheatersService } from './services/theaters.service';
import { UsersService } from './services/users.service';

@NgModule({
  providers: [
    AuthService,
    StorageService,
    NotificatorService,
    ReviewsService,
    TheatersService,
    UsersService,
    AuthGuard,
  ],
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
  ],
  exports: [HttpClientModule],
})
export class CoreModule {}
