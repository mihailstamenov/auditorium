import { ReviewsService } from '../../core/services/reviews.service';
import { NotificatorService } from '../../core/services/notificator.service';
import { TheatersService } from '../../core/services/theaters.service';
import { TheaterDTO } from '../models/theater.dto';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-view-theater',
  templateUrl: './view-theater.component.html',
  styleUrls: ['./view-theater.component.css'],
})
export class ViewTheaterComponent implements OnInit {
  public theater: TheaterDTO;
  panelOpenState = false;
  public theaterId: number;
  public addReviewForm: FormGroup;

  constructor(
    private readonly theatersService: TheatersService,
    private readonly formBuilder: FormBuilder,
    private readonly notification: NotificatorService,
    private readonly reviewService: ReviewsService
  ) {}

  ngOnInit(): void {}
}
