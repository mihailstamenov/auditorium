export class TheaterDTO {
  public id: number;

  public name: string;

  public city: string;

  public streetAddress: string;

  public url: string;

  public starRating: number;
}
