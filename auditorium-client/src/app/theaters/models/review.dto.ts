export class ReviewDTO {
  public id: number;

  public comment: string;

  public datePublished: Date;

  public starRating: number;
}
