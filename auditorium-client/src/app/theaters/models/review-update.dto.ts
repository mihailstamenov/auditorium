export class UpdateReviewDTO {
  public comment?: string;

  public starRating?: number;

  public recommended?: boolean;
}
