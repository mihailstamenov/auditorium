export class AddReviewDTO {
  public comment: string;

  public starRating: number;

  public recommended: boolean;
}
