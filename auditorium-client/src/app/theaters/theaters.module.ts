import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllTheatersComponent } from './all-theaters/all-theaters.component';
import { SharedModule } from '../shared/shared.module';

import { ViewTheaterComponent } from './view-theater/view-theater.component';
import { ReviewsListComponent } from '../theaters/reviews-list/reviews-list.component';
import { TheaterInfoComponent } from './theater-info/theater-info.component';

@NgModule({
  declarations: [
    AllTheatersComponent,
    ViewTheaterComponent,
    ReviewsListComponent,
    TheaterInfoComponent,
  ],
  imports: [CommonModule, SharedModule],
  exports: [AllTheatersComponent, ViewTheaterComponent],
})
export class TheatersModule {}
