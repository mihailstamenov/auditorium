import { Component, OnInit } from '@angular/core';
import { TheaterDTO } from '../models/theater.dto';
import { TheatersService } from 'src/app/core/services/theaters.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-theater-info',
  templateUrl: './theater-info.component.html',
  styleUrls: ['./theater-info.component.css'],
})
export class TheaterInfoComponent implements OnInit {
  public theater: TheaterDTO;

  public theaterId: string;

  constructor(
    private readonly theatersService: TheatersService,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.theaterId = this.route.snapshot.params.theaterId;
    this.theatersService
      .getTheater(this.theaterId)
      .subscribe((data: TheaterDTO) => {
        this.theater = data;
      });
  }
}
