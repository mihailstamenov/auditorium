import { ReviewsService } from '../../core/services/reviews.service';
import { ReviewDTO } from '../models/review.dto';
import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reviews-list',
  templateUrl: './reviews-list.component.html',
  styleUrls: ['./reviews-list.component.css'],
})
export class ReviewsListComponent implements OnInit {
  public reviews: ReviewDTO[];
  public theaterId: string;
  constructor(
    private readonly reviewsService: ReviewsService,
    private readonly route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.theaterId = this.route.snapshot.params.theaterId;

    this.reviewsService
      .getTheaterReviews(this.theaterId)
      .subscribe((data: ReviewDTO[]) => {
        this.reviews = data;
      });
  }

  public refresh(reviewId: number): void {
    // this.reviewsService
    //   .getAllReviews(this.idOftheater)
    //   .subscribe((foundReviews) => {
    //     this.reviews = foundReviews;
    //   });
  }
}
