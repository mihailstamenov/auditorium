import { TheatersService } from '../../core/services/theaters.service';
import { TheaterDTO } from '../models/theater.dto';
import { Component, OnInit } from '@angular/core';
import { ReviewsService } from 'src/app/core/services/reviews.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-theaters',
  templateUrl: './all-theaters.component.html',
  styleUrls: ['./all-theaters.component.css'],
})
export class AllTheatersComponent implements OnInit {
  public selectedTheater: TheaterDTO;
  public theaters: TheaterDTO[];
  constructor(
    private readonly theatersService: TheatersService,
    private readonly reviewsService: ReviewsService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.theatersService.getAllTheaters().subscribe((data: TheaterDTO[]) => {
      this.theaters = data;
    });
  }
  public onSelect(theater: TheaterDTO): void {
    console.log(theater);
    this.selectedTheater = theater;
    console.log(this.selectedTheater);
    this.router.navigate(['/theaters', theater.id]);
  }
}
