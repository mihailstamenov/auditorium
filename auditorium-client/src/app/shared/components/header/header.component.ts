import { AuthService } from '../../../core/services/auth.service';
import { UserDTO } from '../../../users/models/user.dto';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatToolbarModule } from '@angular/material/toolbar';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  public keyword = '';

  public loggedUserData: UserDTO;
  private loggedUserSubscription: Subscription;

  constructor(
    private readonly router: Router,
    public readonly authService: AuthService
  ) {}

  public ngOnInit(): void {
    // this.loggedUserSubscription = this.authService.loggedUser$.subscribe(
    //   (data: UserDTO) => (this.loggedUserData = data)
    // );
  }

  public searchPost(): void {
    // this.router.navigate(['/posts/search', this.keyword]);
  }

  public searchPostWithEnter(event): void {
    // if (event.which === 13) {
    //   this.searchPost();
    // }
  }

  public logout(): void {
    // this.authService.logOutUser();
    // this.loggedUserData = undefined;
  }
}
